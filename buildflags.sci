function cflags = addincludepath(cflags, path)
	if ~isempty(path) then
		cflags = cflags + " -I" + path;
	end
endfunction

function ldflags = addlibpathwin(ldflags, path)
	if ~isempty(path) then
		ldflags = ldflags + " /LIBPATH:" + path;
	end
endfunction

function ldflags = addlibpath(ldflags, path)
	if ~isempty(path) then
		ldflags = ldflags + " -L" + path;
	end
endfunction

function cflags = getCompilationFlags()
	if getos() <> "Windows" then
		cflags = " -g";
	else
		cflags = "";
	end
	cflags = addincludepath(cflags, getenv('SALOME_MED_INC', ''));
	cflags = addincludepath(cflags, getenv('MED_INC', ''));
	cflags = addincludepath(cflags, getenv('HDF5_INC', ''));
endfunction

function ldflags = getLinkFlags()	
	ldflags = "";
	if getos() <> "Windows" then		
		ldflags = addlibpath(ldflags, getenv('SALOME_MED_LIB', ''));
		ldflags = addlibpath(ldflags, getenv('MED_LIB', ''));
		ldflags = addlibpath(ldflags, getenv('HDF5_LIB', ''));		
		ldflags = ldflags + " -lmedcoupling -lmedloader -lmed -lhdf5";
	else
		ldflags = addlibpathwin(ldflags, getenv('SALOME_MED_LIB', ''));
		ldflags = addlibpathwin(ldflags, getenv('MED_LIB', ''));
		ldflags = addlibpathwin(ldflags, getenv('HDF5_LIB', ''));
		ldflags = ldflags + " medcoupling.lib medloader.lib medC.lib";
		if getenv('DEBUG_SCILAB_DYNAMIC_LINK', 'NO') <> 'YES'
			ldflags = ldflags + " hdf5.lib";
		else
			ldflags = ldflags + " hdf5d.lib";
		end
	end
endfunction
