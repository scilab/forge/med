SWIG = @SWIG@
SWIG_LIB = @SWIG_LIB@
export SWIG_LIB
SCILAB = @SCILAB@
SCILABSTARTOPT = @SCILABSTARTOPT@

SALOME_MED_INC = @SALOME_MED_INC@
export SALOME_MED_INC
MED_INC = @MED_INC@
export MED_INC
HDF5_INC = @HDF5_INC@
export HDF5_INC
SALOME_MED_LIB = @SALOME_MED_LIB@
export SALOME_MED_LIB
MED_LIB = @MED_LIB@
export MED_LIB
HDF5_LIB = @HDF5_LIB@
export HDF5_LIB

MODULE = MED

INTERFACE = swig/$(MODULE).i

INCLUDES = -I$(SALOME_MED_INC) -I$(MED_INC) -I$(CURDIR)/src
SRCS = DataArrayHelper.cxx

CFLAGS = "-I\" + builddir + \"/src"

SWIG_OPT = -v

LD_LIBRARY_PATH=.:$(SALOME_MED_LIB):$(MED_LIB):$(HDF5_LIB)
export LD_LIBRARY_PATH

export HDF5_DISABLE_VERSION_CHECK=2

BUILDER = builder.sce
LOADER = loader.sce

TOOLBOX_VERSION = 1.1
TOOLBOX_NAME = $(MODULE)_$(TOOLBOX_VERSION)
TOOLBOX_DIR = toolbox/$(TOOLBOX_NAME)
TOOLBOX_WINDOWS_DIR = toolbox_windows/$(TOOLBOX_NAME)

all: build

$(BUILDER):
	$(SWIG) -scilab -c++ -o $(MODULE)_wrap.cxx $(SWIG_OPT) -builder -builderflagscript buildflags.sci -buildercflags $(CFLAGS) -buildersources $(SRCS) -builderverbositylevel 2 $(INCLUDES) $(INTERFACE);

swig: $(BUILDER)

$(LOADER): swig
	$(SCILAB) $(SCILABSTARTOPT) -f $(BUILDER);

build: $(LOADER)

run: build
	$(SCILAB) $(SCILABSTARTOPT) -f scripts/run.sce

test: build
	$(SCILAB) $(SCILABSTARTOPT) -f scripts/test.sce

debug: build
	$(SCILAB) $(SCILABSTARTOPT) -debug -f scripts/run.sce

toolbox: build
	@mkdir -p $(TOOLBOX_DIR)
	@cp -r toolbox_skeleton/* $(TOOLBOX_DIR)
	@cp libMED.so $(TOOLBOX_DIR)/sci_gateway
	@cp loader.sce loader_gateway.sce
	@sed -i 's/loader.sce/loader_gateway.sce/g' loader_gateway.sce
	@mv -f loader_gateway.sce $(TOOLBOX_DIR)/sci_gateway
	@mkdir $(TOOLBOX_DIR)/tests
	@cp -rf tests/unit_tests $(TOOLBOX_DIR)/tests
	@cd toolbox && tar -czf $(TOOLBOX_NAME).tar.gz $(TOOLBOX_NAME)

toolbox_windows: swig
	@mkdir -p $(TOOLBOX_WINDOWS_DIR)
	@cp -r toolbox_skeleton/* $(TOOLBOX_WINDOWS_DIR)
	@cp buildflags.sci $(TOOLBOX_WINDOWS_DIR)
	@cp builder.sce $(TOOLBOX_WINDOWS_DIR)
	@cp -rf src $(TOOLBOX_WINDOWS_DIR)
	@cp *.cxx $(TOOLBOX_WINDOWS_DIR)
	@mkdir $(TOOLBOX_DIR)/tests
	@cp -rf tests/unit_tests $(TOOLBOX_WINDOWS_DIR)/tests

clean_toolbox:
	@rm -rf toolbox

clean_toolbox_windows:
	@rm -rf toolbox

clean:
	@rm -rf *.sce *.so lib*.c *_wrap.cxx toolbox toolbox_windows

distclean:
	@rm -rf *.sce *.so lib*.c *_wrap.cxx toolbox configure config.log config.status makefile setenv.sh
