%apply (int* IN, int IN_ROWCOUNT, int IN_COLCOUNT) { (int* values, int nbTuples, int nbComponents) }
%apply (double* IN, int IN_ROWCOUNT, int IN_COLCOUNT) { (double* values, int nbTuples, int nbComponents) }

%apply (int** OUT, int* OUT_ROWCOUNT, int* OUT_COLCOUNT) { (int** values, int* nbTuples, int* nbComponents) }
%apply (double** OUT, int* OUT_ROWCOUNT, int* OUT_COLCOUNT) { (double** values, int* nbTuples, int* nbComponents) }

%inline
%{
extern void IArray_setValues(ParaMEDMEM::DataArrayInt *dataArrayInt, int* values, int nbTuples, int nbComponents);
extern void DArray_setValues(ParaMEDMEM::DataArrayDouble *dataArrayDouble, double* values, int nbTuples, int nbComponents);
extern void IArray_getValues(ParaMEDMEM::DataArrayInt *dataArrayInt, int** values, int* nbTuples, int* nbComponents);
extern void DArray_getValues(ParaMEDMEM::DataArrayDouble *dataArrayDouble, double** values, int* nbTuples, int* nbComponents);
%}

