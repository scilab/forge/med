%include "scilist.swg"
%include "scipairint.swg"


%typemap(in, fragment="SWIG_ScilabList", fragment="SWIG_GetIntPair") std::vector<std::pair<int, int> >& (std::vector<std::pair<int, int> > temp)
%{
  SciErr sciErr;
  int *piListAddr = NULL;
  int iListSize;

  if (SWIG_GetScilabListAndSize($input, &piListAddr, &iListSize) != SWIG_OK)
  {
    return SWIG_ERROR;
  }

  $1 = &temp;
  $1->reserve(iListSize);
  for (int i = 0; i < iListSize; i++)
  {
    int* piChildList = NULL;

    sciErr = getListItemAddress(pvApiCtx, piListAddr, i + 1, &piChildList);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return SWIG_ERROR;
    }

    int values[2];
    if (SWIG_GetIntPair($input, piChildList, values) == SWIG_OK)
    {
      $1->push_back(std::pair<int,int>(values[0], values[1]));
    }
    else
    {
      return SWIG_ERROR;
    }
  }
%}

%typemap(out, fragment="SWIG_FromIntPair") std::vector<std::pair<int, int> >
%{
{
  SciErr sciErr;
  int *piList = NULL;
  int iVarOut = SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition();

  int nbItems = $1.size();
  sciErr = createList(pvApiCtx, iVarOut, nbItems, &piList);
  if(sciErr.iErr)
  {
    printError(&sciErr, 0);
    return SWIG_ERROR;
  }

  int* piData = NULL;
  for (int i = 0; i < nbItems; i++)
  {
    int *piChildList = NULL;
    sciErr = createMListInList(pvApiCtx, iVarOut, piList, i + 1, 4, &piChildList);
    if(sciErr.iErr)
    {
      printError(&sciErr, 0);
      return SWIG_ERROR;
    }

    std::pair<int, int> p = $1.at(i);
    int values[2] = {p.first, p.second};
    if (SWIG_FromIntPair(iVarOut, piChildList, values) != SWIG_OK)
    {
      return SWIG_ERROR;
    }
  }

  SWIG_Scilab_SetOutput(pvApiCtx, iVarOut);
}
%}

%typemap(out) std::vector<ParaMEDMEM::TypeOfField>
%{
{
  SciErr sciErr;
  double *pdValues = NULL;
  int iVarOut = SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition();
  int size = $1.size();
  int i;

  pdValues = (double*) malloc(size * sizeof(double));
  for (i = 0; i < size; i++)
    pdValues[i] = $1.at(i);

  sciErr = createMatrixOfDouble(pvApiCtx, SWIG_NbInputArgument(pvApiCtx) + SWIG_Scilab_GetOutputPosition(), 1, size, pdValues);
  if (sciErr.iErr) {
    printError(&sciErr, 0);
    free(pdValues);
    return SWIG_ERROR;
  }

  free(pdValues);
  SWIG_Scilab_SetOutput(pvApiCtx, iVarOut);
}
%}
