%typemap(typecheck) (int *BEGIN, int *END)
{
}

%typemap(in) (int *BEGIN, int *END)
{
    int nbRows = 0;
    int nbCols = 0;
    if (SWIG_SciDoubleOrInt32_AsIntArrayAndSize(pvApiCtx, $input, &nbRows, &nbCols, &$1, SWIG_Scilab_GetFuncName()) != SWIG_ERROR)
    {
        $2 = $1 + nbRows * nbCols;
    }
    else
    {
        return SWIG_ERROR;
    }
}

%typemap(in, noblock=1, numinputs=0) ParaMEDMEM::DataArrayInt *& 
{
}

%typemap(arginit, noblock=1) ParaMEDMEM::DataArrayInt *& ( ParaMEDMEM::DataArrayInt* temp )
{
    temp = ParaMEDMEM::DataArrayInt::New();
    $1 = &temp;
}

%typemap(argout, noblock=1) ParaMEDMEM::DataArrayInt *& {
    %set_output(SWIG_NewPointerObj(%as_voidptr(*$1), $descriptor, 0));
}
