%module MED

%{
// undefine Scilab macros (defined in stack-c.h) which provoke conflict
#undef getType
#undef getPrecision
#undef Max
#undef Min

#undef VERSION

#ifdef _MSC_VER
#pragma warning(disable: 4290)
#endif

#include "NormalizedUnstructuredMesh.hxx"
#include "MEDCouplingRefCountObject.hxx"

#include "MEDCouplingMemArray.hxx"
#include "MEDCouplingMemArray.hxx"
#include "MEDCouplingUMesh.hxx"
#include "MEDCouplingExtrudedMesh.hxx"
#include "MEDCouplingCMesh.hxx"
#include "MEDCouplingField.hxx"
#include "MEDCouplingFieldDouble.hxx"
#include "MEDCouplingFieldTemplate.hxx"
#include "MEDCouplingGaussLocalization.hxx"
#include "MEDCouplingMultiFields.hxx"
#include "MEDCouplingFieldOverTime.hxx"
#include "MEDCouplingDefinitionTime.hxx"

#include "MEDLoader.hxx"
#include "MEDFileMesh.hxx"
#include "MEDFileField.hxx"
#include "MEDFileData.hxx"
#include "SauvReader.hxx"
#include "SauvWriter.hxx"

using namespace ParaMEDMEM;
using namespace INTERP_KERNEL;

%}

%include typemaps.i
%include matrix.i

// STL

%rename(assign) *::operator=;
%ignore *::operator+;
%ignore *::operator+=;
%ignore *::operator-;
%ignore *::operator-=;
%ignore *::operator*;
%ignore *::operator*=;
%ignore *::operator/;
%ignore *::operator/=;
%ignore *::operator==;
%ignore *::operator++;
%ignore *::operator--;
%ignore *::operator!=;
%ignore *::operator^;
%ignore *::operator^=;

%ignore swig::SciSwigIterator;

%include stl.i
%include std_set.i


// Ignore some very specific functions and
// to not exceed the Scilab limit of 1000 gateways

%ignore *::buildEuclidianDistanceDenseMatrix;
%ignore *::buildEuclidianDistanceDenseMatrixWith;
%ignore *::GetPosOfItemGivenBESRelativeNoThrow;
%ignore *::getTinySerializationIntInformation;
%ignore *::getTinySerializationStrInformation;
%ignore *::getTinySerializationDblInformation;
%ignore *::checkDeepEquivalOnSameNodesWith;
%ignore *::getCellIdsFullyIncludedInNodeIds;
%ignore *::tryToShareSameCoordsPermute;
%ignore *::findAndCorrectBadOriented3DExtrudedCells;
%ignore *::arePolyhedronsNotCorrectlyOriented;
%ignore *::are2DCellsNotCorrectlyOriented;
%ignore *::MergeNodesOnUMeshesSharingSameCoords;
%ignore *::getRenumArrForConsecutiveCellTypesSpec;
%ignore *::getTimeDiscretizationUnderGround;
%ignore *::checkDeepEquivalOnSameNodesWith;
%ignore *::FindCorrespCellByNodalConn;
%ignore *::TryToCorrectPolyhedronOrientation;
%ignore *::AggregateSortedByTypeMeshesOnSameCoords;
%ignore *::findAndCorrectBadOriented3DExtrudedCells;
%ignore *::buildDescendingConnectivity2;
%ignore *::buildNewNumberingFromCommonNodesFormat;
%ignore *::isEqualWithoutConsideringStr;
%ignore *::isEqualWithoutConsideringStrAndOrder;
%ignore *::BuildOld2NewArrayFromSurjectiveFormat2;
%ignore *::IArray_setContigPartOfSelectedValues2;
%ignore *::invertArrayO2N2N2O;
%ignore *::invertArrayN2O2O2N;
%ignore *::GetPosOfItemGivenBESRelativeNoThrow;
%ignore *::getComponentsInfo;

%ignore *::checkNoNullValues;
%ignore *::getMinMaxPerComponent;
%ignore *::maxPerTupleWithCompoID;
%ignore *::renumberInPlaceR;
%ignore *::doublyContractedProduct;
%ignore *::buildPermArrPerLevel;
%ignore *::checkMonotonic;
%ignore *::computeNbOfInteractionsWith;
%ignore *::sumPerTuple;
%ignore *::reprQuickOverview;

//Salome 7.2 need to ignore more
%ignore DataArrayChar;
%ignore DataArrayCharTuple;
%ignore DataArrayAsciiChar;
%ignore DataArrayAsciiCharTuple;
%ignore DataArrayAsciiCharIterator;
%ignore DataArrayByte;
%ignore DataArrayByteTuple;
%ignore DataArrayByteIterator;
%ignore DataArrayIntTuple;
%ignore DataArrayDoubleTuple;

%ignore *::checkCoherency1;
%ignore *::checkCoherency2;
%ignore *::setPartOfValues1;
%ignore *::setPartOfValues2;
%ignore *::setPartOfValues3;
%ignore *::setPartOfValues4;
%ignore *::setPartOfValuesSimple1;
%ignore *::setPartOfValuesSimple2;
%ignore *::setPartOfValuesSimple3;
%ignore *::setPartOfValuesSimple4;

%ignore *::checkDeepEquivalOnSameNodesWith;
%ignore *::getCellIdsFullyIncludedInNodeIds;
%ignore *::checkTypeConsistencyAndContig;
%ignore *::accumulatePerChunck;
%ignore *::getHeapMemorySizeWithoutChildren;
%ignore *::pushBackSilent;
%ignore *::popBackSilent;
%ignore *::setContigPartOfSelectedValues;
%ignore *::setContigPartOfSelectedValues2;
%ignore *::computeTupleIdsNearTuples;
%ignore *::computeOffsets;
%ignore *::computeOffsets2;
%ignore *::findRangeIdForEachTuple;
%ignore *::findIdInRangeForEachTuple;
%ignore *::searchRangesInListOfIds;
%ignore *::buildExplicitArrByRanges;
%ignore *::buildExplicitArrOfSliceOnScaledArr;
%ignore *::partitionByDifferentValues;
%ignore *::FindPermutationFromFirstToSecond;
%ignore *::BuildOld2NewArrayFromSurjectiveFormat2;
%ignore *::getNumberOfTuplesExpectedRegardingCode;
%ignore *::getCellIdsHavingGaussLocalization;
%ignore *::getGaussLocalizationIdsOfOneType;
%ignore *::getGaussLocalizationIdOfOneCell;
%ignore *::getDirectChildren;
%ignore *::getWeightedAverageValue;
%ignore *::ReadFieldsGaussNEOnSameMesh;;
%ignore *::buildUnionOf2DMesh;
%ignore *::buildUnionOf3DMesh;
%ignore *::ComputeRangesFromTypeDistribution;
%ignore *::computeIsoBarycenterOfNodesPerCell;
%ignore *::Is3DExtrudedStaticCellWellOriented;
%ignore *::rearrange2ConsecutiveCellTypes;
%ignore *::ComputeNeighborsOfCellsAdv;
%ignore *::getBoundingBoxForBBTree2DQuadratic;
%ignore *::getBoundingBoxForBBTree;
%ignore *::getBoundingBoxForBBTreeFast;
%ignore *::findAndCorrectBadOriented3DCells;
%ignore *::convertNodalConnectivityToStaticGeoTypeMesh;
%ignore *::computePlaneEquationOf3DFaces;
%ignore *::getCellIdsCrossingPlane;
%ignore *::IsPolygonWellOriented;
%ignore *::setGaussLocalizationOnType;
%ignore *::buildSetInstanceFromThis;
%ignore *::buildFacePartOfMySelfNode;
%ignore *::AssignStaticWritePropertiesTo;
%ignore *::substractInPlaceDM;
%ignore *::GetAllPossibilitiesStr;
%ignore *::buildStructuredSubPart;
%ignore *::checkDeepEquivalWith;
%ignore *::ComputeSpreadZoneGradually;
%ignore *::ComputeVecAndPtOfFace;
%ignore *::SimplifyPolyhedronCell;
%ignore *::getRenumArrForMEDFileFrmt;
%ignore *::getLevArrPerCellTypes;
%ignore *::buildPartOrthogonalField;
%ignore *::buildSpreadZonesWithPoly;
%ignore *::partitionBySpreadZone;
%ignore *::splitInBalancedSlices;
%ignore *::duplicateEachTupleNTimes;

//ignore for compilations errors (TOFIX)
%ignore MEDCouplingSizeOfVoidStar;
%ignore *::getHeapMemorySize;

%ignore *::EPS_FOR_POLYH_ORIENTATION;

// Wrap constants and enums to Scilab variables
%scilabconst(1);

//===============
// INTERP Kernel
//===============

%typemap(throws) INTERP_KERNEL::Exception
%{
  Scierror(999, "An exception of type INTERP_KERNEL::Exception has been thrown.\n%s\n", _e.what());
  SWIG_fail;
%}

%include "NormalizedUnstructuredMesh.hxx"
%include "NormalizedGeometricTypes"

//=============
// MEDCoupling
//=============

#define MEDCOUPLING_EXPORT

%include "MEDCouplingTimeLabel.hxx"
%include "MEDCouplingRefCountObject.hxx"

// Instantiate needed templates but do not wrap it
namespace std
{
    %template() vector<int>;
    %template() vector<double>;
    %template() vector<string>;
    %template() set<int>;
}

//===================
// DataArray classes
//===================

%rename(Array) DataArray;
%rename(IArray) DataArrayInt;
%rename(DArray) DataArrayDouble;

%rename(setComponentInfo) setInfoOnComponent;
%rename(setComponentsInfo) setInfoOnComponents;
%rename(getComponentInfo) getInfoOnComponent;
%rename(getComponentsInfo) getInfoOnComponents;


%include "MEDCouplingTypemaps.i"

%include "MEDCouplingMemArray.hxx"

%include "swig/DataArrayHelper.i"

//==============
// Mesh classes
//==============

%apply std::set<int> &OUTPUT { std::vector<int>& conn };

%apply (int IN_SIZE, int *IN) { (int size, const int *nodalConnOfCell) };
%apply (int *BEGIN, int *END) { (const int *begin, const int *end) };
%apply (int *BEGIN, int *END) { (const int *cellIdsBg, const int *cellIdsEnd) };

// Mesh

%rename(STATIC_UNSTRUCTURED) SINGLE_STATIC_GEO_TYPE_UNSTRUCTURED;
%rename(DYNAMIC_UNSTRUCTURED) SINGLE_DYNAMIC_GEO_TYPE_UNSTRUCTURED;
%rename(Mesh) MEDCouplingMesh;
%include "MEDCouplingMesh.hxx"

// PSet
%rename(PtSet) MEDCouplingPointSet;
%include "MEDCouplingPointSet.hxx"

%rename(UMeshCellEntry) MEDCouplingUMeshCellEntry;

// UMesh
%rename(UMesh) MEDCouplingUMesh;
%rename(nodalConnectivity) getNodalConnectivity;
%rename(nodalConnectivityIdx) getNodalConnectivityIndex;
%include "MEDCouplingUMesh.hxx"

// EMesh
%rename(EMesh) MEDCouplingExtrudedMesh;
%include "MEDCouplingExtrudedMesh.hxx"

// CMesh
%rename(CMesh) MEDCouplingCMesh;
%include "MEDCouplingCMesh.hxx"

//===============
// Field classes
//===============

%include "MEDCouplingNatureOfFieldEnum"

%apply (int *BEGIN, int *END) { (const int *startCellIds, const int *endCellIds) };

%rename(Field) MEDCouplingField;
%include "MEDCouplingField.hxx";

%rename(DField) MEDCouplingFieldDouble;
%apply double *OUTPUT { double *res };
%include "MEDCouplingFieldDouble.hxx";

%rename(FieldTmpl) MEDCouplingFieldTemplate;
#include "MEDCouplingFieldTemplate.hxx"

%rename(GaussLoc) MEDCouplingGaussLocalization;
#include "MEDCouplingGaussLocalization.hxx"

%rename(MFields) MEDCouplingMultiFields;
#include "MEDCouplingMultiFields.hxx"

%rename(TimeField) MEDCouplingFieldOverTime;
#include "MEDCouplingFieldOverTime.hxx"

%rename(DefTime) MEDCouplingDefinitionTime;
#include "MEDCouplingDefinitionTime.hxx"

//===========
// MEDLoader
//===========

#define MEDLOADER_EXPORT

%rename("MED") MEDLoader;

%rename(GetMeshOnGroupFamiliesNames) GetMeshFamiliesNamesOnGroup;
%rename(GetMeshOnFamilyGroupNames) GetMeshGroupsNamesOnFamily;

// Instantiate needed templates but do not wrap it
namespace std
{
    %template() vector<ParaMEDMEM::MEDCouplingUMesh const*>;
    %template() vector<ParaMEDMEM::MEDCouplingFieldDouble*>;
}

%include "MEDLoaderTypemaps.i"

%include "MEDLoader.hxx"

