AC_INIT([MED], [1.0])

# Arguments
AC_ARG_WITH(swig, [  --with-swig=path        location of Swig executable], [WITH_SWIG="$withval"], [WITH_SWIG="no"])
AC_ARG_WITH(scilab, [  --with-scilab=path      location of Scilab executable], [WITH_SCILAB="$withval"], [WITH_SCILAB="no"])
AC_ARG_WITH(salome_med_inc, [  --with-salome_med_inc=path     location of Salome MED include], [WITH_SALOME_MED_INC="$withval"], [WITH_SALOME_MED_INC="no"])
AC_ARG_WITH(salome_med_lib, [  --with-salome_med_lib=path     location of Salome MED library], [WITH_SALOME_MED_LIB="$withval"], [WITH_SALOME_MED_LIB="no"])
AC_ARG_WITH(med, [  --with-med=path     location of med library], [WITH_MED="$withval"], [WITH_MED="no"])
AC_ARG_WITH(hdf5, [  --with-hdf5=path     location of HDF5 library], [WITH_HDF5="$withval"], [WITH_HDF5="no"])

SWIG=
SWIG_LIB=
SCILAB=
SCILABSTARTOPT=
SALOME_MED_CFLAGS=
SALOME_MED_LIB=
SALOME_MED_LDFLAGS=
MED_CFLAGS=
MED_LIB=
MED_LDFLAGS=
HDF5_CFLAGS=
HDF5_LIB=
HDF5_LDFLAGS=

# Check for compilers and standard includes
AC_PROG_CC
AC_PROG_CXX

# Check for SWIG
if test "x$WITH_SWIG" = xno; then
  AC_CHECK_PROGS(SWIG, swig)
else
  AC_MSG_CHECKING(for swig)
  if test -f "$WITH_SWIG"; then
    AC_MSG_RESULT($WITH_SWIG)
    SWIG="$WITH_SWIG"
  else
    AC_MSG_ERROR(Swig not found)
  fi
fi

# Check for SWIG Lib
AC_MSG_CHECKING(for SWIG Lib)
SWIG_PATH="`dirname \"$SWIG\"`"
SWIG_PATH="`(cd \"$SWIG_PATH\" && pwd)`"
if test -r $SWIG_PATH/Lib; then
  SWIG_LIB=$SWIG_PATH/Lib
  AC_MSG_RESULT($SWIG_LIB)
else
  AC_MSG_ERROR($SWIG_PATH/Lib not found)
fi

# Check for Scilab
if test "x$WITH_SCILAB" = xno; then
  AC_CHECK_PROGS(SCILAB, scilab)
else
  AC_MSG_CHECKING(for scilab)
  if test -f "$WITH_SCILAB"; then
    AC_MSG_RESULT($WITH_SCILAB)
    SCILAB="$WITH_SCILAB"
  else
    AC_MSG_ERROR(Scilab not found)
  fi
fi

if test -n "$SCILAB"; then
  # Check for Scilab version (needs api_scilab so needs version 5.2 or higher)
  SCILAB_FULL_VERSION=`$SCILAB -version|head -1|sed -e 's|Scilab version \"\(.*\)\"|\1|g'`
  AC_MSG_NOTICE([Scilab version: $SCILAB_FULL_VERSION])

  AC_MSG_CHECKING(for Scilab version is 5.2 or higher)
  SCILAB_MAJOR_VERSION=`echo $SCILAB_FULL_VERSION | cut -d. -f1`
  SCILAB_MINOR_VERSION=`echo $SCILAB_FULL_VERSION | cut -d. -f2`
  SCILAB_VERSION="$SCILAB_MAJOR_VERSION$SCILAB_MINOR_VERSION"

  if test $SCILAB_VERSION -ge 52; then
    AC_MSG_RESULT(yes)
  else
    AC_MSG_ERROR(Scilab 5.2 or higher needed)
  fi

  # Set Scilab startup options depending on version
  AC_MSG_CHECKING(for Scilab startup options)
  SCILABSTARTOPT="-nwni -nb"
  if test $SCILAB_VERSION -ge 54; then
    SCILABSTARTOPT+=" -noatomsautoload"
  fi
  AC_MSG_RESULT($SCILABSTARTOPT)
fi

# Check for Salome MED include
if test "x$WITH_SALOME_MED_INC" = xno; then
  AC_CHECK_HEADERS([MEDCoupling.hxx MEDLoader.hxx])
else
  AC_MSG_CHECKING(for Salome MED include)
  if test -r $WITH_SALOME_MED_INC; then
    AC_MSG_RESULT($WITH_SALOME_MED_INC)
    SALOME_MED_INC=$WITH_SALOME_MED_INC
  else
    AC_MSG_ERROR($WITH_SALOME_MED_INC not found)
  fi
fi

# Check for Salome MED lib
if test "x$WITH_SALOME_MED_LIB" = xno; then
  AC_CHECK_LIB([medcoupling], [])
  AC_CHECK_LIB([medloader], [])
else
  AC_MSG_CHECKING(for Salome MED lib)
  if test -r $WITH_SALOME_MED_LIB; then
    AC_MSG_RESULT($WITH_SALOME_MED_LIB)
    SALOME_MED_LIB=$WITH_SALOME_MED_LIB
  else
    AC_MSG_ERROR($WITH_SALOME_MED_LIB not found)
  fi
fi

# Check for med
if test "x$WITH_MED" = xno; then
  AC_CHECK_HEADER(med.h)
  AC_CHECK_LIB(med, [MEDfamCr])
else
  AC_MSG_CHECKING(for med include)
  if test -r $WITH_MED/include; then
    AC_MSG_RESULT($WITH_MED/include)
    MED_INC=$WITH_MED/include
  else
    AC_MSG_ERROR($WITH_MED/include not found)
  fi

  AC_MSG_CHECKING(for med lib)
  if test -r $WITH_MED/lib; then
    AC_MSG_RESULT($WITH_MED/lib)
    MED_LIB=$WITH_MED/lib
  else
    AC_MSG_ERROR($WITH_MED/lib not found)
  fi
fi

# Check for HDF5
if test "x$WITH_HDF5" = xno; then
  AC_CHECK_HEADER(hdf5.h)
  AC_CHECK_LIB(hdf5, [H5Pcreate])
else
  AC_MSG_CHECKING(for hdf5 include)
  if test -r $WITH_HDF5/include; then
    AC_MSG_RESULT($WITH_HDF5/include)
    HDF5_INC=$WITH_HDF5/include
  else
    AC_MSG_ERROR($WITH_HDF5/include not found)
  fi

  AC_MSG_CHECKING(for hdf5 lib)
  if test -r $WITH_HDF5/lib; then
    AC_MSG_RESULT($WITH_HDF5/lib)
    HDF5_LIB=$WITH_HDF5/lib
  else
    AC_MSG_ERROR($WITH_HDF5/lib not found)
  fi
fi

AC_SUBST(SWIG)
AC_SUBST(SWIG_LIB)
AC_SUBST(SCILAB)
AC_SUBST(SCILABSTARTOPT)
AC_SUBST(SALOME_MED_INC)
AC_SUBST(SALOME_MED_LIB)
AC_SUBST(MED_INC)
AC_SUBST(MED_LIB)
AC_SUBST(HDF5_INC)
AC_SUBST(HDF5_LIB)

# Generate files
AC_CONFIG_FILES([Makefile setenv.sh])
AC_OUTPUT
