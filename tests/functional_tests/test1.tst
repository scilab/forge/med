function values_by_comp = getValuesFromMED(file_name)
	values_by_comp = struct();
	
	mesh_names = MED_GetMeshNames(file_name);

	for i = 1:size(mesh_names, '*') 
		mesh = MED_ReadUMeshFromFile(file_name, mesh_names(i), 0);
		mesh_name = Mesh_getName(mesh);
		field_names = MED_GetAllFieldNamesOnMesh(file_name, mesh_name);
		
		for j = 1:size(field_names, '*')
		    field_name = field_names(i);
		    field_type = MED_GetTypesOfField(file_name, mesh_name, field_name); 
		    field_iterations = MED_GetFieldIterations(ON_NODES, file_name, mesh_name, field_name);
		    last_iteration_idx = size(field_iterations);
		  
		    field = MED_ReadField(field_type, file_name, mesh_name, 0, field_name, field_iterations(last_iteration_idx).first, field_iterations(last_iteration_idx).second);
		    d_array = DField_getArray(field);
		    nb_components = Array_getNumberOfComponents(d_array);
		    nb_tuples = Array_getNumberOfTuples(d_array);
		    values = DArray_getValues(d_array);  
		    for k = 1:nb_components
		        comp_name = Array_getComponentInfo(d_array, k-1);
		        values_by_comp(comp_name) = values(:, k);
		    end
	   end     
	end
endfunction


MED_Init();

valuesA = getValuesFromMED("test1a.med")
valuesB = getValuesFromMED("test1b.med")


