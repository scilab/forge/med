// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Simon MARCHETTO
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

exec(fullfile(MEDGetRootPath(), "tests/unit_tests/testUtils.sci"));

dvalues = [0.8,10.8; 1.9,11.9; 2.1,12.1; 3.2,13.2; 4.3,14.3; 5.4,15.4; 6.5,16.5];

function testDArray_array()
    array = createDArray(dvalues);
    checkDArrayValues(array, dvalues);
endfunction

function testDArray_getNumberOfTuples()
    array = createDArray(dvalues);
    assert_checkequal(Array_getNumberOfTuples(array), 7);
endfunction

function testDArray_getNumberOfComponents()
    array = createIArray(dvalues);
    assert_checkequal(Array_getNumberOfComponents(array), 2);
endfunction

function testDArray_getNbOfElems()
    array = createIArray(dvalues);
    assert_checkequal(Array_getNbOfElems(array), 14);
endfunction

function testDArray_repr()
    array = createDArray([1.0; 2.0; 3.0; 4.0]);
    repr = DArray_repr(array);
    expectedRepr = ['Name of int array : """"'; ..
                   'Number of components : 1'; ..
                   'Info of these components : """"'; ..
                   'Number of tuples : 4'; ..
                   'Data content :'; ..
                   'Tuple #0 : 1'; ..
                   'Tuple #1 : 2'; ..
                   'Tuple #2 : 3'; ..
                   ''];
    assert_checkequal(strsplit(repr, ascii(10)), expectedRepr);
endfunction

function testDArray_fillWithValue()
    array = DArray_New();
    DArray_alloc(array, 3, 2);
    DArray_fillWithValue(array, 7.0);
    checkDArrayValues(array, [7.0, 7.0; 7.0, 7.0; 7.0, 7.0]);
endfunction

function testDArray_substr()
    array = createDArray(dvalues);
    array2 = DArray_substr(array, 3);
    checkDArrayValues(array2, dvalues(3:7, :));
    array3 = DArray_substr(array, 2, 5);
    checkDArrayValues(array3, dvalues(2:5, :));
endfunction

function testDArray_convert()
    darray = createDArray(dvalues);
    iarray = DArray_convertToIntArr(darray);
    ivalues = [0,10; 1,11; 2,12; 3,13; 4,14; 5,15; 6,16];
    checkIArrayValues(iarray, ivalues);
endfunction

testDArray_array();
testDArray_getNumberOfTuples();
testDArray_getNumberOfComponents();
testDArray_getNbOfElems();
//testDArray_repr();
testDArray_fillWithValue();
//testDArray_substr();
testDArray_convert();
