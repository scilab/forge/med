// Factories

function array = createIArray(values)
    array = IArray_New();
    ivalues = values;
    IArray_setValues(array, ivalues);
endfunction

function array = createDArray(values)
    array = DArray_New();
    DArray_setValues(array, values);
endfunction

function array = createDArrayWithInfos(values, infos)
    array = DArray_New();
    DArray_setValues(array, values);
    if ~isempty(infos) then
        for i = 1:size(infos, '*')
            Array_setComponentInfo(array, i-1, infos(i));
        end
    end
endfunction

function [mesh, nbCells] = createUMesh(name, meshDimension, cellTypes, cellNodes)
    mesh = UMesh_New();
    Mesh_setName(mesh, name);
    UMesh_setMeshDimension(mesh, meshDimension);

    nbCells = size(cellNodes);
    UMesh_allocateCells(mesh, nbCells);

    for i = 1:nbCells
        UMesh_insertNextCell(mesh, cellTypes(i), cellNodes(i));
    end
    UMesh_finishInsertingCells(mesh);
endfunction

function mesh = createUMeshWithCoords(name, meshDimension, cellTypes, cellNodes, nodeCoords, infos)
    mesh = createUMesh(name, meshDimension, cellTypes, cellNodes);

    darray = createDArrayWithInfos(nodeCoords, infos);
    PtSet_setCoords(mesh, darray);

    Mesh_checkCoherency(mesh);
endfunction

function mesh = createCMesh(coordsX, coordsY, coordsZ)
    mesh = CMesh_New();
    arrX = createDArray(coordsX);
    arrY = createDArray(coordsY);
    if ~isempty(coordsZ)
        arrZ = createDArray(coordsZ);
        CMesh_setCoords(mesh, arrX, arrY, arrZ);
    else
        CMesh_setCoords(mesh, arrX, arrY);
    end
endfunction

function field = createDField(name, cellType, timeType, mesh)
    field = DField_New(cellType, timeType);
    Field_setMesh(field, mesh);
    Field_setName(field, name);
endfunction


function pair = createIntPair(first, second)
    pair = struct("first", int32(first), "second", int32(second));
endfunction


// Checks

function checkIArrayValues(array, expectedValues)
    values = IArray_getValues(array);
    assert_checkequal(values, expectedValues);
endfunction

function checkDArrayValues(array, expectedValues)
    values = DArray_getValues(array);
    assert_checkalmostequal(values, expectedValues, 1e-14);
endfunction


// Samples

function [mesh, nbCells] = createQuad4Mesh()
    nbCells = 6;
    cellNodes = list( ..
               [1, 2,  8,  7], ..
               [2, 3,  9,  8], ..
               [3, 4, 10,  9], ..
               [4, 5, 11, 10], ..
               [5, 0,  6, 11], ..
               [0, 1,  7,  6]);
    cellTypes = repmat(NORM_QUAD4, 1, nbCells);
    mesh = createUMesh("Quad4Mesh", 2, cellTypes, cellNodes);
endfunction

function [mesh, nbCells, coords] = createQuad4MeshWithCoords()
    [mesh, nbCells] = createQuad4Mesh();
    coords = [0.024155,             0.04183768725682622,   -0.305; ..
            0.04831000000000001, -1.015761910347357e-17, -0.305; ..
            0.09662000000000001, -1.832979297858306e-18, -0.305; ..
            0.120775,             0.04183768725682623,   -0.305; ..
            0.09662000000000001,  0.08367537451365245,   -0.305; ..
            0.04831000000000001,  0.08367537451365246,   -0.305; ..
            0.024155,             0.04183768725682622,   -0.2863; ..
            0.04831000000000001, -1.015761910347357e-17, -0.2863; ..
            0.09662000000000001, -1.832979297858306e-18, -0.2863; ..
            0.120775,             0.04183768725682623,   -0.2863; ..
            0.09662000000000001,  0.08367537451365245,   -0.2863; ..
            0.04831000000000001,  0.08367537451365246,   -0.2863];
    darray = createDArray(coords);
    PtSet_setCoords(mesh, darray);
endfunction

function mesh = createCMesh2D()
    coordsX = [-1.0; 1.0; 2.0; 4.0];
    coordsY = [-2.0; 2.0; 4.0; 8.0];
    mesh = createCMesh(coordsX, coordsY, []);
endfunction

function mesh = createCMesh3D()
    coordsX = [-1.0; 1.0; 2.0; 4.0];
    coordsY = [-2.0; 2.0; 4.0; 8.0];
    coordsZ = [-3.0; 3.0; 6.0; 12.0];
    mesh = createCMesh(coordsX, coordsY, coordsZ);
endfunction

function mesh = createUMesh1D()
    cellTypes = [NORM_SEG2, NORM_SEG2, NORM_SEG2, NORM_SEG3];
    cellNodes = list([0,1], [1,2], [2,3], [3,4,5]);
    nodeCoords = [0.0; 0.3; 0.75; 1.0; 1.4; 1.3];
    mesh = createUMeshWithCoords("UMesh1D", 1, cellTypes, cellNodes, nodeCoords, ["comp1"]);
endfunction

function mesh = createUMesh2DCurve()
    cellTypes = [NORM_SEG2, NORM_SEG2, NORM_SEG2, NORM_SEG3];
    cellNodes = list([0,1], [1,2], [2,3], [3,4,5]);
    nodeCoords = [0.0,0.0; 0.3,0.3; 0.75,0.75; 1.0,1.0; 1.4,1.4; 1.3,1.3];
    mesh = createUMeshWithCoords("UMesh2DCurve", 1, cellTypes, cellNodes, nodeCoords, ["comp1", "comp2"]);
endfunction

function mesh = createUMesh2D()
    cellTypes = [NORM_TRI3, NORM_TRI3, NORM_TRI6, NORM_QUAD4, NORM_QUAD4, NORM_POLYGON];
    cellNodes = list([1,4,2], [4,5,2], [6,10,8,9,11,7], [0,3,4,1], [6,7,4,3], [7,8,5,4]);
    nodeCoords = [-0.3,-0.3; 0.2,-0.3; 0.7,-0.3; -0.3,0.2; 0.2,0.2; 0.7,0.2; -0.3,0.7; 0.2,0.7; 0.7,0.7; ..
        -0.05,0.95; 0.2,1.2; 0.45,0.95];
    mesh = createUMeshWithCoords("UMesh2D", 2, cellTypes, cellNodes, nodeCoords, ["comp1", "comp2"]);
endfunction

function mesh = createUMesh3DSurf()
    cellTypes = [NORM_TRI3, NORM_TRI3, NORM_TRI6, NORM_QUAD4, NORM_QUAD4, NORM_POLYGON];
    cellNodes = list([1,4,2], [4,5,2], [6,10,8,9,11,7], [0,3,4,1], [6,7,4,3], [7,8,5,4]);
    nodeCoords = [-0.3,-0.3,-0.3; 0.2,-0.3,-0.3; 0.7,-0.3,-0.3; -0.3,0.2,-0.3; 0.2,0.2,-0.3; 0.7,0.2,-0.3; ..
        -0.3,0.7,-0.3; 0.2,0.7,-0.3; 0.7,0.7,-0.3; -0.05,0.95,-0.3; 0.2,1.2,-0.3; 0.45,0.95,-0.3];
    mesh = createUMeshWithCoords("UMesh3DSurf", 2, cellTypes, cellNodes, nodeCoords, ["comp1", "comp2"]);
endfunction

function mesh = createUMesh3D()
    cellTypes = [NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8, NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8, NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8, NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8, NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8, NORM_HEXA8, ..
        NORM_POLYHED, ..
        NORM_HEXA8];
    cellNodes = list([0,11,1,3,15,26,16,18], ..
        [1,2,4,7,13,6,-1,1,16,21,6,-1,6,21,28,13,-1,13,7,22,28,-1,7,4,19,22,-1,4,2,17,19,-1,2,1,16,17,-1,16,21,28,22,19,17], ..
        [1,6,5,3,16,21,20,18], [13,10,9,6,28,25,24,21], ..
        [11,8,7,4,2,1,-1,11,26,16,1,-1,1,16,17,2,-1,2,17,19,4,-1,4,19,22,7,-1,7,8,23,22,-1,8,11,26,23,-1,26,16,17,19,22,23], ..
        [7,12,14,13,22,27,29,28], [15,26,16,18,30,41,31,33], ..
        [16,17,19,22,28,21,-1,16,31,36,21,-1,21,36,43,28,-1,28,22,37,43,-1,22,19,34,37,-1,19,17,32,34,-1,17,16,31,32,-1,31,36,43,37,34,32], ..
        [16,21,20,18,31,36,35,33],   [28,25,24,21,43,40,39,36], ..
        [26,23,22,19,17,16,-1,26,41,31,16,-1,16,31,32,17,-1,17,32,34,19,-1,19,34,37,22,-1,22,23,38,37,-1,23,26,41,38,-1,41,31,32,34,37,38], ..
        [22,27,29,28,37,42,44,43], [30,41,31,33,45,56,46,48], ..
        [31,32,34,37,43,36,-1,31,46,51,36,-1,36,51,58,43,-1,43,37,52,58,-1,37,34,49,52,-1,34,32,47,49,-1,32,31,46,47,-1,46,51,58,52,49,47], ..
        [31,36,35,33,46,51,50,48],  [43,40,39,36,58,55,54,51], ..
        [41,38,37,34,32,31,-1,41,56,46,31,-1,31,46,47,32,-1,32,47,49,34,-1,34,49,52,37,-1,37,38,53,52,-1,38,41,56,53,-1,56,46,47,49,52,53], ..
        [37,42,44,43,52,57,59,58]);
    nodeCoords = [0.,0.,0.; 1.,1.,0.; 1.,1.25,0.; 0.,1.,0.; 1.,1.5,0.; 2.,0.,0.; 2.,1.,0.; 1.,2.,0.; 0.,2.,0.; 3.,1.,0.; ..
        3.,2.,0.; 0.,1.,0.; 1.,3.,0.; 2.,2.,0.; 2.,3.,0.; ..
        0.,0.,1.; 1.,1.,1.; 1.,1.25,1.; 0.,1.,1.; 1.,1.5,1.; 2.,0.,1.; 2.,1.,1.; 1.,2.,1.; 0.,2.,1.; 3.,1.,1.; ..
        3.,2.,1.; 0.,1.,1.; 1.,3.,1.; 2.,2.,1.; 2.,3.,1.; ..
        0.,0.,2.; 1.,1.,2.; 1.,1.25,2.; 0.,1.,2.; 1.,1.5,2.; 2.,0.,2.; 2.,1.,2.; 1.,2.,2.; 0.,2.,2.; 3.,1.,2.; ..
        3.,2.,2.; 0.,1.,2.; 1.,3.,2.; 2.,2.,2.; 2.,3.,2.; ..
        0.,0.,3.; 1.,1.,3.; 1.,1.25,3.; 0.,1.,3.; 1.,1.5,3.; 2.,0.,3.; 2.,1.,3.; 1.,2.,3.; 0.,2.,3.; 3.,1.,3.; ..
        3.,2.,3.; 0.,1.,3.; 1.,3.,3.; 2.,2.,3.; 2.,3.,3.];
    infos = ["dist (m)", "density [MW/m^3]", "t [kW]"];
    mesh = createUMeshWithCoords("UMesh3D", 3, cellTypes, cellNodes, nodeCoords, infos);
endfunction

// create 4 meshes from a set of cells, we obtain 4 families and 4 groups
function meshes = createMultiMesh()
    // create a mesh "UMesh3D"
    mesh = createUMesh3D();
    // create a "sub mesh of UMesh3D" (built with some cells used for "UMesh3D")
    cells = int32([1, 2, 4, 13, 15]);
    submesh1 = UMesh_buildPartOfMySelf(mesh, cells, %T);
    Mesh_setName(submesh1, "submesh1");
    // create another "sub mesh of UMesh3D"
    cells = int32([3, 4, 13, 14]);
    submesh2 = UMesh_buildPartOfMySelf(mesh, cells, %T);
    Mesh_setName(submesh2, "submesh2");
    // create another "sub mesh of UMesh3D"
    coords = PtSet_getCoords(mesh);
    submesh3 = createUMesh("submesh3", 3, [NORM_TETRA4], list([0, 11, 1, 3]));
    PtSet_setCoords(submesh3, coords);

    meshes = list(mesh, submesh1, submesh2, submesh3);
endfunction

function [field, fieldName, mesh] = createDFieldOnCells()
    mesh = createUMesh3DSurf();
    fieldName = "fieldOnCells";
    field = createDField(fieldName, ON_CELLS, ONE_TIME);
    dvalues = [0.,10.,20.; 1.,11.,21.; 2.,12.,22.; 3.,13.,23.; 4.,14.,24.; 5.,15.,25.];
    infos = ["x", "y", "z"];
    darray = createDArrayWithInfos(dvalues, infos);
    DField_setArray(field, darray);
endfunction

// Initialize toolbox
MED_Init();
