// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Simon MARCHETTO
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

exec(fullfile(MEDGetRootPath(), "tests/unit_tests/testUtils.sci"));

function testUMesh_create()
    mesh = UMesh_New();
    assert_checkequal(Mesh_getType(mesh), UNSTRUCTURED);
endfunction

function testUMesh_name()
    mesh = UMesh_New();
    Mesh_setName(mesh, "mesh1");
    assert_checkequal(Mesh_getName(mesh), "mesh1");
endfunction

function testUMesh_getNumberOfCells()
    [mesh, nbCells] = createQuad4Mesh();
    assert_checkequal(Mesh_getNumberOfCells(mesh), nbCells);
endfunction

function testUMesh_NodalConnectivity()
    mesh = createQuad4Mesh();
    nodalConnectivity = UMesh_nodalConnectivity(mesh);
    assert_checkequal(Array_getNbOfElems(nodalConnectivity), 30);
endfunction

function testUMesh_NodalConnIndex()
    [mesh, nbCells] = createQuad4Mesh();
    nodalConnectivityIndex = UMesh_nodalConnectivityIdx(mesh);
    assert_checkequal(Array_getNbOfElems(nodalConnectivityIndex), nbCells+1);
endfunction

function testUMesh_getSpaceDimension()
    mesh = createQuad4MeshWithCoords();
    assert_checkequal(Mesh_getMeshDimension(mesh), 2);
endfunction

function testUMesh_getMeshDimension()
    mesh = createQuad4MeshWithCoords();
    assert_checkequal(Mesh_getMeshDimension(mesh), 2);
endfunction

function testUMesh_getNumberOfNodes()
    mesh = createQuad4MeshWithCoords();
    assert_checkequal(Mesh_getNumberOfNodes(mesh), 12);
endfunction

function testUMesh_coords()
    [mesh, nbCells, coords] = createQuad4MeshWithCoords();
    darray = PtSet_getCoords(mesh);
    checkDArrayValues(darray, coords);
endfunction

function testUMesh_getDistTypes()
    mesh = UMesh_New();
    UMesh_setMeshDimension(mesh, 2);
    UMesh_allocateCells(mesh, 3);

    UMesh_insertNextCell(mesh, NORM_QUAD4, [1, 2, 3, 4]);
    UMesh_insertNextCell(mesh, NORM_TRI3, [1, 2, 5]);
    UMesh_insertNextCell(mesh, NORM_TRI3, [1, 2, 6]);
    UMesh_finishInsertingCells(mesh);

    types = UMesh_getDistributionOfTypes(mesh);
    // 1 cell of type QUAD4 (4), 2 cells of type TRI3 (3)
    expectedTypes = [4, 1, -1, 3, 2, -1];
    assert_checkequal(types, expectedTypes);
endfunction

function testUMesh_buildPartOfMySelf()
    mesh = createQuad4MeshWithCoords();
    sub_mesh = UMesh_buildPartOfMySelf(mesh, [1, 2, 3], %T);
    // TODO
endfunction

function testUMesh_getAllGeoTypes()
    mesh = createQuad4MeshWithCoords();
    types = Mesh_getAllGeoTypes(mesh);
    // TODO
endfunction


testUMesh_create();
testUMesh_name();
testUMesh_getNumberOfCells();
testUMesh_getNumberOfNodes();
testUMesh_NodalConnectivity();
testUMesh_NodalConnIndex();
testUMesh_getSpaceDimension();
testUMesh_getMeshDimension();
testUMesh_coords();
testUMesh_getDistTypes();
