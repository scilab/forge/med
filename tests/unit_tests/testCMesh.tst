// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Simon MARCHETTO
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

exec(fullfile(MEDGetRootPath(), "tests/unit_tests/testUtils.sci"));

function testCMesh_create()
    mesh = CMesh_New();
    assert_checkequal(CARTESIAN, CMesh_getType(mesh));
endfunction

function testCMesh_name()
    mesh = CMesh_New();
    Mesh_setName(mesh, "mesh1");
    assert_checkequal(Mesh_getName(mesh), "mesh1");
endfunction

function testCMesh_getSpaceDimension()
    mesh = createCMesh3D();
    assert_checkequal(CMesh_getSpaceDimension(mesh), 3);
endfunction

function testCMesh_getMeshDimension()
    mesh = createCMesh3D();
    assert_checkequal(CMesh_getMeshDimension(mesh), 3);
endfunction

function testCMesh_getNumberOfCells()
    mesh = createCMesh3D();
    assert_checkequal(CMesh_getNumberOfCells(mesh), 27);
endfunction

function testCMesh_getNumberOfNodes()
    mesh = createCMesh3D();
    assert_checkequal(CMesh_getNumberOfNodes(mesh), 64);
endfunction

function testCMesh_getNodeIdsOfCell()
    mesh = createCMesh2D();
    nodeIds = Mesh_getNodeIdsOfCell(mesh, 1);
    assert_checkequal(nodeIds, [1, 2, 6, 5]);
endfunction


function testCMesh_fillFromAnalyticOnNodes()
    mesh = createCMesh3D();
    fieldOnNodes = Mesh_fillFromAnalytic(mesh, ON_NODES, 1, "x+y/2.0+z/3.0");

    assert_checkequal(DField_getNumberOfComponents(fieldOnNodes), 1);
    assert_checkequal(DField_getNumberOfTuples(fieldOnNodes), 64);

    expectedValues = [-3.; -1.; 0.; 2.; -1.; 1.; 2.; 4.; 0.; 2.; 3.; 5.; 2.; 4.; 5.; 7.; -1.; 1.; 2.; ..
                4.; 1.; 3.; 4.; 6.; 2.; 4.; 5.; 7.; 4.; 6.; 7.; 9.; 0.; 2.; 3.; 5.; 2.; 4.; 5.; ..
                7.; 3.; 5.; 6.; 8.; 5.; 7.; 8.; 10.; 2.; 4.; 5.; ..
                7.; 4.; 6.; 7.; 9.; 5.; 7.; 8.; 10.; 7.; 9.; 10.; 12.];
    darray = DField_getArray(fieldOnNodes);
    checkDArrayValues(darray, expectedValues);
endfunction

function testCMesh_fillFromAnalyticOnCells()
    mesh = createCMesh3D();
    fieldOnCells = Mesh_fillFromAnalytic(mesh, ON_CELLS, 1, "x+y/2.0+z/3.0");

    assert_checkequal(DField_getNumberOfComponents(fieldOnCells), 1);
    assert_checkequal(DField_getNumberOfTuples(fieldOnCells), 27);

    expectedValues = [0; 1.5; 3; 1.5; 3; 4.5; 3; 4.5; 6; 1.5; 3; 4.5; 3; 4.5; ..
                6; 4.5; 6; 7.5; 3; 4.5; 6; 4.5; 6; 7.5; 6; 7.5; 9];
    darray = DField_getArray(fieldOnCells);
    checkDArrayValues(darray, expectedValues);
endfunction

testCMesh_create();
testCMesh_name();
testCMesh_getSpaceDimension();
testCMesh_getMeshDimension();
testCMesh_getNumberOfCells();
testCMesh_getNumberOfNodes();
testCMesh_fillFromAnalyticOnNodes();
testCMesh_fillFromAnalyticOnCells();
testCMesh_getNodeIdsOfCell();
