// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2013 - Scilab Enterprises - Simon MARCHETTO
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

exec(fullfile(MEDGetRootPath(), "tests/unit_tests/testUtils.sci"));

ivalues1 = [12,11,10,9; 8,7,6,5; 4,3,2,1];
ivalues2 = [0,10; 1,11; 2,12; 3,13; 4,14; 5,15; 6,16];
compInfos1 = ["comp1", "comp2", "comp3", "comp4"];

function testIArray_name()
    array = IArray_New();
    Array_setName(array, "array1");
    assert_checkequal(Array_getName(array), "array1");
endfunction

function testIArray_getNumberOfTuples()
    array = createIArray(ivalues1);
    assert_checkequal(Array_getNumberOfTuples(array), 3);
endfunction

function testIArray_getNumberOfComponents()
    array = createIArray(ivalues1);
    assert_checkequal(Array_getNumberOfComponents(array), 4);
endfunction

function testIArray_getNbOfElems()
    array = createIArray(ivalues1);
    assert_checkequal(Array_getNbOfElems(array), 12);
endfunction

function testIArray_repr()
    array = createIArray([1; 2; 3]);
    repr = IArray_repr(array);
    expectedRepr = ['Name of int array : """"'; ..
                   'Number of components : 1'; ..
                   'Info of these components : """"'; ..
                   'Number of tuples : 3'; ..
                   'Data content :'; ..
                   'Tuple #0 : 1'; ..
                   'Tuple #1 : 2'; ..
                   'Tuple #2 : 3'; ..
                   ''];
    assert_checkequal(strsplit(repr, ascii(10)), expectedRepr);
endfunction

function testIArray_array()
    array = createIArray(ivalues1);
    checkIArrayValues(array, ivalues1);
endfunction

function testIArray_componentInfo()
    array = createIArray(ivalues1);
    for i = 1:size(compInfos1, '*')
        Array_setComponentInfo(array, i-1, compInfos1(i));
    end
    for i = 1:size(compInfos1, '*')
      info = Array_getComponentInfo(array, i-1);
      assert_checkequal(info, compInfos1(i));
    end
endfunction

function testIArray_componentsInfo()
    array = createIArray(ivalues1);
    Array_setComponentsInfo(array, compInfos1);
    infos = Array_getComponentsInfo(array);
    assert_checkequal(infos, compInfos1);
endfunction

function testIArray_substr()
    array = createIArray(ivalues2);
    array2 = IArray_substr(array, 3);
    checkIArrayValues(array2, ivalues2(4:7, :));
    array3 = IArray_substr(array, 2, 5);
    checkIArrayValues(array3, ivalues2(2:5, :));
endfunction

function testIArray_fillWithValue()
    array = IArray_New();
    IArray_alloc(array, 3, 2);
    IArray_fillWithValue(array, 7.0);
    checkIArrayValues(array, [7, 7; 7, 7; 7, 7]);
endfunction

function testIArray_convert()
    iarray = createIArray(ivalues1);
    darray = IArray_convertToDblArr(iarray);
    dvalues = [12.0,11.0,10.0,9.0; 8.0,7.0,6.0,5.0; 4.0,3.0,2.0,1.0];
    checkDArrayValues(darray, dvalues);
endfunction

function testIArray_setSelectedComponents()
    iarray1 = createIArray([1,2; 3,4; 5,6]);
    iarray2 = createIArray([11,12; 13,14; 15,16]);
    IArray_setSelectedComponents(iarray1, iarray2, [0, 1]);
    checkIArrayValues(iarray1, [11,12; 13,14; 15,16]);
endfunction

function testIArray_getDifferentValues()
    iarray = createIArray([1, 2, 2, 3, 3, 3]);
    iarrayUniqueValues = IArray_getDifferentValues(iarray);
    checkIArrayValues(iarrayUniqueValues, [1; 2; 3]);
endfunction


testIArray_name();
testIArray_getNumberOfTuples();
testIArray_getNumberOfComponents();
testIArray_getNbOfElems();
testIArray_array();
//testIArray_repr();
testIArray_componentInfo();
//testIArray_componentsInfo();
//testIArray_substr();
testIArray_fillWithValue();
testIArray_convert();
testIArray_setSelectedComps();
testIArray_getDifferentValues();
