function path = MEDGetRootPath()
  path = [];
  if isdef('MEDlib') then
    [macros, macrosPath] = libraryinfo('MEDlib');
    path = pathconvert(fullpath(macrosPath + "../"), %t, %t);
  end
endfunction